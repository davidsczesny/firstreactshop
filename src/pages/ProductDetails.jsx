import React, { useContext } from "react";
import { useParams, useLocation } from "react-router-dom";
import logo from '../logo.svg';
import cart from '../cart.svg';
import nodejs from '../nodejs.svg';
import Sidebar from '../components/Sidebar';
import Cart from '../components/Cart';
import ProductDetailsContent from '../components/ProductDetailsContent';
import CartContext from "../CartContext";

const ProductDetails = () => {
    let location = useLocation();
    const { cart: cartItems } = useContext(CartContext);
    return (
        <>
            <main id='main'>
                <header id='header'>
                    <img id='logo' src={logo} alt="" />
                    <button id='cart-logo' onClick={() => document.getElementById("app").toggleAttribute("cart")}>
                        <img src={cart} alt="" />
                        {cartItems.length > 0 ? <span>{cartItems.length}</span> : null}
                    </button>
                </header>
                <section id='container'>
                    <aside id='banner' onClick={() => document.getElementById("app").toggleAttribute("sidebar")}>
                        <img src={nodejs} alt="" />
                        <span className='sideways'>Get BaD Swag for <strong>FREE</strong></span>
                        <button>OPEN SIDEBAR</button>
                    </aside>
                    <Sidebar />
                    <ProductDetailsContent data={location.state} />
                </section>
            </main>
            <Cart />
        </>

    );
}

export default ProductDetails;