import React, { useContext } from "react";
import logo from '../logo.svg';
import cart from '../cart.svg';
import nodejs from '../nodejs.svg';
import Sidebar from '../components/Sidebar';
import Cart from '../components/Cart';
import Content from '../components/Content';
import CartContext from "../CartContext";

const Home = ({ open }) => {
    const { cart: cartItems } = useContext(CartContext);
    const { isOpen, setOpen } = open;
    return (
        <>
            <main id='main'>
                <header id='header'>
                    <img id='logo' src={logo} alt="" />
                    <button id='cart-logo' onClick={e => setOpen({ ...isOpen, cart: !isOpen.cart })}>
                        <img src={cart} alt="" />
                        {cartItems.length > 0 ? <span>{cartItems.length}</span> : null}
                    </button>
                </header>
                <section id='container'>
                    <aside id='banner' onClick={e => setOpen({ ...isOpen, sidebar: !isOpen.sidebar })}>
                        <img src={nodejs} alt="" />
                        <span className='sideways'>Get BaD Swag for <strong>FREE</strong></span>
                        <button>OPEN SIDEBAR</button>
                    </aside>
                    <Sidebar open={open} />
                    <Content />
                </section>
            </main>
            <Cart />
        </>

    );
}

export default Home;