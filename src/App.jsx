import './global.css';
import './App.css';
import { Routes, Route, Link } from "react-router-dom";
import Home from './pages/Home';
import Product from './pages/ProductDetails';
import React from 'react';

const App = () => {
  const [isOpen, setOpen] = React.useState({ cart: false, sidebar: false });
  return (
    <div id="app" {...isOpen.cart ? { cart: "" } : null}  {...isOpen.sidebar ? { sidebar: "" } : null}>
      <Routes>
        <Route path="/" element={<Home open={{ isOpen, setOpen }} />} />
        <Route path="product/:productId" element={<Product />} />
      </Routes>
    </div >
  );
}

export default App;