import React, { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import CartContext from "../CartContext";

const ProductDetailsContent = ({ data }) => {
    let navigate = useNavigate();
    const [quantity, setQuantity] = useState(1);
    const [variant, setVariant] = useState("");
    const [errors, setErrors] = useState({});
    const { setCart } = useContext(CartContext);

    const handleAddToCart = () => {
        let error = {};
        if (variant === "")
            error.variant = "Please select a variant";
        if (quantity <= 0)
            error.quantity = "Please enter a valid quantity";
        setErrors(error);
        if (variant === "" || quantity <= 0)
            return;
        setCart(cart => [...cart, { product: data, quantity, variant, id: (performance.now().toString(36) + Math.random().toString(36)).replace(/\./g, "") }]);
    }

    return (
        <article id='content'>
            <div className="center">
                <section className="details">
                    <section className="left">
                        <div className="productImg">
                            <img src={data.image} alt="" />
                        </div>
                    </section>
                    <section className="right">
                        <button onClick={() => navigate("/")}>
                            <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path></svg>
                            Back to Product List
                        </button>
                        <div>
                            <h1>{data.title}</h1>
                            <p className="description">{data.description}</p>
                            <h2>USD ${data.price}</h2>
                        </div>
                        {
                            Object.keys(errors).length > 0 && <ul id="errorMessage">
                                {Object.values(errors).map((error, i) => <li key={i}>{error}</li>)}
                            </ul>
                        }
                        <form noValidate onSubmit={e => e.preventDefault()}>
                            <fieldset>
                                <label htmlFor="quantity">Qty.</label>
                                <br />
                                <input type="number" id="quantity" name="quantity" min="1" max="100" step="1" value={quantity} onChange={e => setQuantity(e.target.value)} />
                            </fieldset>
                            <fieldset>
                                <label htmlFor="variant">Size/Color</label>
                                <br />
                                <select id="variant" name="variant" value={variant} required onChange={e => setVariant(e.target.value)}>
                                    <option disabled value="">Choose Size/Color</option>
                                    <option value="SM - Women">SM — Women</option>
                                    <option value="MD - Women">MD — Women</option>
                                    <option value="LG - Women">LG — Women</option>
                                    <option value="XL - Women">XL - Women</option>
                                    <option value="SM - Unisex">SM - Unisex</option>
                                    <option value="MD - Unisex">MD - Unisex</option>
                                    <option value="LG - Unisex">LG —&nbsp;Unisex</option>
                                    <option value="XL - Unisex">XL — Unisex</option>
                                    <option value="2XL - Unisex">2XL — Unisex</option>
                                    <option value="3XL - Unisex">3XL- Unisex</option>
                                    <option value="2XL - Womens">2XL- Womens</option>
                                </select>
                            </fieldset>
                            <button onClick={handleAddToCart}>Add to Cart<svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M7 18c-1.1 0-1.99.9-1.99 2S5.9 22 7 22s2-.9 2-2-.9-2-2-2zM1 2v2h2l3.6 7.59-1.35 2.45c-.16.28-.25.61-.25.96 0 1.1.9 2 2 2h12v-2H7.42c-.14 0-.25-.11-.25-.25l.03-.12.9-1.63h7.45c.75 0 1.41-.41 1.75-1.03l3.58-6.49c.08-.14.12-.31.12-.48 0-.55-.45-1-1-1H5.21l-.94-2H1zm16 16c-1.1 0-1.99.9-1.99 2s.89 2 1.99 2 2-.9 2-2-.9-2-2-2z"></path></svg></button>
                        </form>
                    </section>
                </section>
            </div>
        </article >
    );
}

export default ProductDetailsContent;