import React, { useContext } from "react";
import CartContext from "../CartContext";
import CartItem from "./CartItem";

const Cart = () => {
    const { cart, setCart } = useContext(CartContext);
    var total = 0;
    cart.forEach(item => {
        total += item.product.price * item.quantity;
    });
    total = Math.round(total * 100) / 100;
    return (
        <aside id='cart'>
            <header>
                <h1>Your Cart</h1>
            </header>
            <div>
                {cart.map((e, i) => {
                    return (
                        <CartItem key={i} data={e}></CartItem>
                    );
                })}
                <p id="total">TOTAL PRICE:  USD ${total}</p>
                <button id="check-out">Check out <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg></button>
            </div>
        </aside>
    );
}

export default Cart;

/*
<h1>{cart.length}</h1>
            {cart.map((e, i) => {
                const { product, quantity, variant } = e;
                return (
                    <div key={i} className='cart-item'>
                        <img height="50px" src={product.image} alt={product.name} />
                        <div className='cart-item-details'>
                            <h3>{product.name}</h3>
                            <p>{product.price}</p>
                        </div>
                    </div>
                );
            })}
*/