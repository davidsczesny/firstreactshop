import React from "react";
import ProductList from './ProductList';

const Content = () => {
    return (
        <article id='content'>
            <h1>
                <header id='title'>
                    <h1>Get BaD Swag!</h1>
                    <p>The money we charge for swag helps to cover production and shipping costs. In the unlikely event that BaD swag ends up turning a profit, we’ll reinvest that money into the open source community.</p>
                </header>
                <ProductList />
            </h1>
        </article>
    );
}

export default Content;