import React from "react";

const Sidebar = ({ open }) => {
    const { isOpen, setOpen } = open;
    return (
        <aside id='sidebar' onClick={e => setOpen({ ...isOpen, sidebar: !isOpen.sidebar })}>
            <div className="content">
                <h3>For Existing Contributors</h3>
                <h2>Get BaD Swag for <strong>FREE!</strong></h2>
                <p>Already contributed to Gatsby? Claim your personal coupon code and get free swag by logging in with your GitHub account!</p>
                <h3>For Future Contributors</h3>
                <h2>Never contributed to Gatsby?</h2>
                <p>Let's get you started with your first contribution to Gatsby! Once you've had your first pull request merged into Gatsby, you can come back here to claim free swag.</p>
                <p>Due to COVID-19 related international mail service disruptions, your order may be delayed or suspended. Please view the list of affected countries to see if your order is affected.</p>
            </div>
        </aside>
    );
}

export default Sidebar;