import React, { useContext } from "react";
import CartContext from "../CartContext";

const CartItem = ({ data }) => {
    const { cart, setCart } = useContext(CartContext);
    const { product, quantity, variant } = data;
    let title = product.title;
    if (title.length > 20) {
        title = title.substring(0, 20);
        title = title.substring(0, Math.min(title.length, title.lastIndexOf(" "))) + "...";
    }
    return (<div className='cart-item'>
        <span>
            <img src={product.image} alt={product.title} />
        </span>
        <h3>
            {quantity}x {title}
            <br />
            {variant}, ${product.price}
        </h3>
        <div className='cart-item-details'>
            {/*<p>USD ${product.price}</p>*/}
            <button className="remove" onClick={e => {
                e.preventDefault();
                const newCart = cart.filter(item => item.id !== data.id);
                setCart(newCart);
            }}>
                <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z"></path></svg>
            </button>
        </div>
    </div>)
}

export default CartItem;