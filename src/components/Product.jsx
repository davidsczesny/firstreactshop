import React from "react";
import { useNavigate } from "react-router-dom";


const Product = ({ data }) => {
    let description = data.description;
    if (description.length > 90) {
        description = description.substring(0, 90) + "...";
    }
    let title = data.title;
    if (title.length > 37) {
        title = title.substring(0, 37);
        title = title.substring(0, Math.min(title.length, title.lastIndexOf(" "))) + "...";
    }
    let navigate = useNavigate();
    return (
        <>
            <section className="top">
                <img src={data.image} alt="" />
            </section>
            <section className="bottom">
                <h1>{title}</h1>
                <p className="description">{description}</p>
                <div className="purchaseInfo">
                    <p className="price">{"USD " + new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(parseFloat(data.price))}</p>
                    <button onClick={() => navigate(`/product/${data.id}`, { state: data })}>
                        <span>
                            view details
                            <br />
                            &amp; buy
                            <svg stroke="currentColor" fill="currentColor" strokeWidth="0" viewBox="0 0 24 24" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M12 4l-1.41 1.41L16.17 11H4v2h12.17l-5.58 5.59L12 20l8-8z"></path></svg>
                        </span>
                    </button>
                </div>
            </section>
        </>
    );
}

export default Product;