import React from 'react';
import { BrowserRouter } from "react-router-dom";
import './global.css';
import App from './App';
import { CartProvider } from './CartContext';
import { createRoot } from 'react-dom/client';

const container = document.getElementById('root');
const root = createRoot(container);
root.render(
  <BrowserRouter>
    <CartProvider><App /></CartProvider>
  </BrowserRouter>
);
